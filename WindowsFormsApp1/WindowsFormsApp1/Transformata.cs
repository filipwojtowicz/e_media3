﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class Transformata
    {
        public int[] vector;

        private const double Pi = 3.14;

        private void Swap(ref int a, ref int b)
        {
            int c = a;
            a = b;
            b = c;
        }
        public void Ftt(int[] data, int number_of_samples, int sample_rate, int sign)
        {
            int n, mmax, m, j, istep, i;
            double wtemp, wr, wpr, wpi, wi, theta, tempr, tempi;


            vector = new int[4 * (sample_rate + 1)];

            for (n = 0; n < sample_rate; n++)
            {
                if (n < number_of_samples)
                    vector[2 * n] = data[n];
                else
                    vector[2 * n] = 0;
                vector[2 * n + 1] = 0;
            }

            n = sample_rate << 1;
            j = 0;
            for (i = 0; i < n / 2; i += 2)
            {
                if (j > i)
                {
                    Swap(ref vector[j], ref vector[i]);
                    Swap(ref vector[j + 1], ref vector[i + 1]);
                    if ((j / 2) < (n / 4))
                    {
                        Swap(ref vector[(n - (i + 2))], ref vector[(n - (j + 2))]);
                        Swap(ref vector[(n - (i + 2)) + 1], ref vector[(n - (j + 2)) + 1]);
                    }
                }
                m = n >> 1;
                while (m >= 2 && j >= m)
                {
                    j -= m;
                    m >>= 1;
                }
                j += m;
            }

            mmax = 2;
            while (n > mmax)
            {
                istep = mmax << 1;
                theta = sign * (2 * Pi / mmax);
                wtemp = Math.Sin(0.5 * theta);
                wpr = -2.0 * wtemp * wtemp;
                wpi = Math.Sin(theta);
                wr = 1.0;
                wi = 0.0;

                for (m = 1; m < mmax; m += 2)
                {
                    for (i = m; i <= n; i += istep)
                    {
                        j = i + mmax;
                        tempr = wr * vector[j - 1] - wi * vector[j];
                        tempi = wr * vector[j] + wi * vector[j - 1];
                        vector[j - 1] = vector[i - 1] - (int)tempr;
                        vector[j] = vector[i] - (int)tempi;
                        vector[i - 1] += (int)tempr;
                        vector[i] += (int)tempi;
                    }
                    wr = (wtemp = wr) * wpr - wi * wpi + wr;
                    wi = wi * wpr + wtemp * wpi + wi;
                }
                mmax = istep;
            }

            long fundamental_frequency = 0;
            for (i = 2; i <= sample_rate; i += 2)
            {
                if ((Math.Pow(vector[i], 2) + Math.Pow(vector[i + 1], 2)) > (Math.Pow(vector[fundamental_frequency], 2) + Math.Pow(vector[fundamental_frequency + 1], 2)))
                {
                    fundamental_frequency = i;
                }
            }

            fundamental_frequency = (long)Math.Floor((float)fundamental_frequency / 2);
        }
    }
}
